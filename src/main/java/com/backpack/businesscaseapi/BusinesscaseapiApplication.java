package com.backpack.businesscaseapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BusinesscaseapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(BusinesscaseapiApplication.class, args);
	}

}
