package com.backpack.businesscaseapi.Controller;

import com.backpack.businesscaseapi.Model.Stop;
import com.backpack.businesscaseapi.Repository.StopRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/stops")
public class StopController {
    @Autowired
    private StopRepository stopRepository;

    @GetMapping(value = "")
    public List<Stop> getAllStop(){
        return (List<Stop>) this.stopRepository.findAll();
    }

    @GetMapping(value = "{stop}")
    public Stop getOneStop(@PathVariable(name = "stop", required = false) Stop stop){
        if(stop == null){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Aucun arrêt avec cet ID !");
        }

        return stop;
    }

    @PostMapping(value = "")
    ResponseEntity<Stop> saveStop(@Valid @RequestBody Stop stop){
        this.stopRepository.save(stop);
        return new ResponseEntity<Stop>(stop, HttpStatus.CREATED);
    }

    @PutMapping(value = "{stop}")
    ResponseEntity<Stop> updateStop(@Valid @RequestBody Stop stop, @PathVariable(name = "stop") Stop stopPath){
        if(stopPath == null){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Aucun arrêt avec cet ID !");
        } else {
            stop.setId(stopPath.getId());
            this.stopRepository.save(stop);
        }

        return new ResponseEntity<Stop>(stop, HttpStatus.OK);
    }

    @DeleteMapping(value = "{stop}")
    void deleteStop(@PathVariable(name = "stop", required = false) Stop stop){
        if(stop == null){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Aucun arrêt avec cet ID !");
        }else {
            stopRepository.delete(stop);
        }
    }
}
