package com.backpack.businesscaseapi.Model;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
@Table(name = "stop")
public class Stop {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_stop")
    private Long id;

    @Column(name = "name", unique = true)
    @NotBlank(message = "Veuillez saisir le nom de l'arrêt")
    private String nom;

    @Column(name = "location")
    @NotBlank(message = "Veuillez saisir la localisation")
    private String location;

    @Column(name = "address_ip")
    @NotBlank(message = "Veuillez saisir la localisation")
    private String ipV4;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getIpV4() {
        return ipV4;
    }

    public void setIpV4(String ipV4) {
        this.ipV4 = ipV4;
    }
}
