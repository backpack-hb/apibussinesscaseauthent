package com.backpack.businesscaseapi.Repository;

import com.backpack.businesscaseapi.Model.Stop;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StopRepository extends CrudRepository<Stop, Long> {
}
